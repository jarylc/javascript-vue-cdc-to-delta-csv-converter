# Crypto.com to Delta CSV Converter

![Crypto.com to Delta CSV Converter](demo.webm)

## Description
An open source CSV converter application that converts a Crypto.com exported CSV to a format that is able to be imported into the Delta crypto-currency portfolio application.

This project is coded in HTML, CSS, Javascript with the help of Bootstrap and VueJS framework.

## Self hosting instructions
### Required dependencies
* yarn or npm
* node
### Project setup and build
```
yarn install
```
#### Compiles and hot-reloads for development and short term hosting
```
yarn serve
```
#### Compiles and minifies for production
```
yarn build
```
